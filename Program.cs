﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Lotto
{
    class Program
    {
        private static int count = 0;

        static void Main(string[] args)
        {
            Random r = new Random();
            int[] target = Lotto(r.Next());
            int targetBonus = r.Next(1, 46);

            int first = 0;
            int second = 0;
            int third = 0;
            int forth = 0;
            int fifth = 0;

            int money = 0;

            while (true)
            {
                StringBuilder sb = new StringBuilder();

                int[] newLotto = Lotto(r.Next());
                int lottoCount = CompareArray(target, newLotto);
                int lottoBonus = r.Next(1, 46);

                switch (lottoCount)
                {
                    case 3: // 5등
                        fifth++;
                        money += 5000;
                        break;
                    case 4: // 4등
                        forth++;
                        money += 50000;
                        break;
                    case 5: // 3등, 2등
                        if (targetBonus == lottoBonus)
                        {
                            second++;
                            money += 50000000;
                        }
                        else
                        {
                            third++;
                            money += 1500000;
                        }
                        break;
                    case 6: // 1등
                        first++;
                        money += 2000000000;
                        break;
                }

                sb.AppendLine();
                sb.Append($"당첨 복권 번호 : {string.Join(", ", target)} - 보너스 : {targetBonus}");
                sb.AppendLine();
                sb.Append($"구매 복권 번호 : {string.Join(", ", newLotto)} - 보너스 : {lottoBonus}");
                sb.AppendLine();
                sb.AppendLine("---------------------------------------");
                sb.Append($"1등 횟수 (당첨금 20억원)  : {first}");
                sb.AppendLine();
                sb.Append($"2등 횟수 (당첨금 5천만원) : {second}");
                sb.AppendLine();
                sb.Append($"3등 횟수 (당첨금 150만원) : {third}");
                sb.AppendLine();
                sb.Append($"4등 횟수 (당첨금 5만원)   : {forth}");
                sb.AppendLine();
                sb.Append($"5등 횟수 (당첨금 5천원)   : {fifth}");
                sb.AppendLine();
                sb.AppendLine();
                sb.Append($"총 당첨금 : {money:N0} 원");
                sb.AppendLine();
                sb.Append($"쓴 돈     : {count * 1000:N0} 원");
                sb.AppendLine();
                sb.AppendLine();
                sb.Append($"복권 {count:N0} 개 구매");


                Console.SetCursorPosition(0, 0);
                Console.WriteLine(sb.ToString());
                count++;

                if (target.SequenceEqual(newLotto))
                {
                    break;
                }
            }

            Console.ReadLine();
        }

        private static int CompareArray(int[] arrayPrimary, int[] arraySecondary)
        {
            int count = 0;

            foreach (int intPrimary in arrayPrimary)
            {
                foreach (int intSecondary in arraySecondary)
                {
                    if (intPrimary == intSecondary)
                    {
                        count++;
                    }
                }
            }

            return count;
        }

        private static int[] Lotto(int seed)
        {
            int[] numbers = new int[6];

            Random r = new Random(seed);

            for (int i = 0; i < 6; i++)
            {
                int randomNumber = r.Next(1, 46);

                while (numbers.Contains(randomNumber))
                {
                    randomNumber = r.Next(1, 46);
                }

                numbers[i] = randomNumber;
            }

            Array.Sort(numbers);

            return numbers;
        }

        static void Update()
        {

        }

        static void Draw()
        {
            Console.SetCursorPosition(0, 0);
            Console.WriteLine(1);
        }
    }
}
